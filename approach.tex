\vspace{-2mm}
\section{Augmenting Missing Key Aspects}
\label{sec:approach}

\begin{figure*}
	\centering
	\includegraphics[width=\textwidth]{CVE7}
	\vspace{-8mm}
	\caption{Approach Overview.}
	\label{fig:approachoverview}
	\vspace{-7mm}
\end{figure*}


\begin{figure}%[htbp!]
	\centering
	\includegraphics[scale=0.8]{fusion}
	\vspace{-3mm}
	\caption{Two model architectures.}
	\label{fig:modelarchitecture}
	\vspace{-6mm}
\end{figure}


Motivated by the missing of key aspects in CVE descriptions, we design a neural-network based approach for predicting the missing aspects based on the known aspects in a CVE description.
We first give an overview of our approach (Section~\ref{sec:approachoverview}) and then describe the design of neural-network classifier (Section~\ref{sec:input}-Section~\ref{sec:output}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Approach Overview}
\label{sec:approachoverview}

We formulate the prediction task as a multi-class classification problem for each aspect.
Considering the severity of the information missing (see Section~\ref{sec:extractionquality}), we predict four aspects: vulnerability type, root cause, attacker type and attack vector.
Each aspect has a corresponding multi-class classifier, and the class labels for each aspect-specific classifier are summarized in Table~\ref{tab:freq}.


As shown in Fig.~\ref{fig:approachoverview}, our approach consists of a training phase and a prediction phrase.
Training phase uses the historical CVE descriptions to train aspect-specific neural-network classifiers.
It first uses the aspect extraction method in Section~\ref{sec:extractionrules} to extract six CVE aspects from the historical CVE descriptions.
Then, we prepare the training data for each aspect $t$ to be predicted.
For each CVE that contains the aspect $t$, a training instance is created with the class label of $t$ (denoted as $label(t)$) as the expected output and the description of rest of aspects $r \in R$ ($1 \leq |R| \leq 5$) (denoted as $desc(r)$) as the input.
From such training data, the neural-network classifier is trained to extract syntactic and semantic features from the input aspect descriptions and capture the intrinsic correlations between these input features and the output class label.


At the prediction phrase, given an unseen CVE description, we first extract the CVE aspects present in the description.
For each missing aspect, the trained aspect-specific classifier takes as input the aspects present in the description and predicts as output the most likely class label of the missing aspect. 


The neural network classifier consists of three layers:
an input layer that represents the input text in a vector representation (e.g., word embedding) (Section~\ref{sec:input});
a neural-network feature extractor that extracts syntactic and semantic features from the input text (Section~\ref{sec:nn});
and an output classifier that makes the prediction based on the extracted features (Section~\ref{sec:output}).
Next, we describe the design of these three layers in details.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\vspace{-0.2cm}
\subsection{Input Text and Representation}
\label{sec:input}

The raw input to the classifier is the textual description of CVE aspects, such as those sentence fragments highlighted in Fig.~\ref{fig:cvedesccomplete} and Fig.~\ref{fig:cvedescmissing}.
In this work, we consider three formats of raw input text:
1) the sequence of separate aspect descriptions in the original appearance order (denoted as \textit{i-ao});
2) the sequence of separate aspect descriptions in a random order (denoted as \textit{i-ar});
3) the original CVE description containing all input aspects (denoted as \textit{i-fu}).
\textit{i-ar} allows us to investigate the impact of the appearance order of CVE aspects, and \textit{i-fu} allows us to investigate the impact of additional sentence parts in the original CVE description.
Additional parts refer mostly to preposition, pronoun and/or determiner that connect separate aspect descriptions into a more complete sentence.


Words are discrete symbols.
As the input to the neural network, they must be represented in vectors.
In this work, we use word embeddings to represent words in a vector space, because many studies~\cite{we1, we2, we3} have shown that word embeddings can capture rich syntactic and semantic features of words in a low-dimensional vector.
We consider both general word embeddings (denoted as $we_g$) and domain-specific word embeddings (denoted as $we_d$).
For general word embeddings, we use word embeddings pre-trained on the corpus of Google News text directly
obtained from the official word2vec website~\cite{googlenews}.
Google News word embedding is learned by continuous skip-gram model~\cite{we4}.
For domain-specific word embeddings, we compile two corpora: one from CVE descriptions and the other from the vulnerability report in SecurityFocus.
We set the vocabulary size at 50,000 and learns domain-specific word embeddings on these two corpora separately using continuous skip-gram model (the Python implementation in Gensim~\cite{we5}).
The output of word embedding learning is a dictionary of words, each of which has a $d$-dimensional vector.
In this work, we set $d$ at 300 as in existing studies~\cite{han2017Learning,gong2019joint,xiao2019embedding}.


Let the input text be a sequence of $N$ words $w_i$ ($1 \leq i \leq N$).
This input text is represented as a $N \times d$ matrix: $i$=$v(w_1) \oplus v(w_2)  \oplus ...\oplus v(w_N)$, where $\oplus$ is vector concatenation and $v(w_i)$ returns the word embedding of the word $w_i$ in the dictionary.
We randomly initialize corresponding word vectors to deal with these Out-of-Vocabulary (OOV) words~\cite{oov2}.
Different CVEs may have different numbers of known CVE aspects.
To keep the model architecture consistent, we set an input aspect as an empty string if the CVE does not contain this input aspect. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\vspace{-2mm}
\subsection{Neural Network Feature Extractors}
\label{sec:nn}

A neural-network feature extractor takes as input the matrix $i$ from the input layer and computes as output a feature vector $o$ which is fed into the output Softmax classifier for prediction.

\subsubsection{Model Architecture}
\label{sec:model}

As our input consists of separate CVE aspects, we design two model architectures to investigate the effective mechanism for incorporating CVE aspects and capturing their intrinsic correlations: early fusion versus late fusion.
As shown in Fig.~\ref{fig:modelarchitecture}, early fusion architecture first concatenates the input matrix of each aspect into one input matrix, which is fed into a single neural network to extract and fuse features from different aspects. 
In contrast, late fusion architecture feeds the input matrix of each aspect into a neural network separately and then fuse the output feature vector of the separate networks by a fully connected layer. 
All neural networks share the same network configuration, but they will learn different weights in different architectures.
Both early fusion and late fusion are applicable to the input formats \textit{i-ao} and \textit{i-ar}.
But only early fusion is applicable to the input format \textit{i-fu}, because \textit{i-fu} merges the input CVE aspects into a whole sentence.


\subsubsection{Backbone Network}
We consider two popular neural networks used for text classification in the literature~\cite{Hassan2018Convolutional,Gurulingappa2012Development,Howard2018Universal}:
Convolutional Neural Network (CNN) and Bi-directional Long-Short Term Memory (BiLSTM).
CNN is good at capturing important words and phrases in the text, while BiLSTM is good at capturing longer-range dependencies in text.
We design various variants of these two networks.
First, we consider shallow (1-layer) versus deep ($\textgreater1$ layer) neural network.
Second, we consider adding  attention layer on top of LSTM layer to weight word importance.
As such, we obtain six neural network models for feature extraction:


\textbf{1-layer CNN:}
Fig.~\ref{fig:cnn} shows 1-layer CNN model, which consists of one convolution layer and a 1-max pooling layer.
The convolution layer applies $M$ filters to the input matrix $i$ of word embeddings.
A filter is a $h \times w$ matrix whose values will be learned during model training.
$h$ is called height or window size.
It refers to the number of consecutive words (e.g., $n$-gram) the filters apply to.
In this work, we use three different window sizes $h$=1, 3, 5. 
That is, the filters extract features from 1-grams, 3-grams and 5-grams respectively.
The width $w$ of filters is set to the dimensionality $d$ of word embeddings because the filters should retain the integrity of the input word embeddings.
For each word window, a filter computes a real value, which is fed into the non-linear activation function ReLU~\cite{relu}: ReLU(x)=max(0,x).
A filter scans the input word sequence (zero-padding at both ends to allow the filter to extract features from the beginning and the end of the input sentence) with stride=1.
This type of filters is also known as 1-d convolution as it scans the input matrix along only one dimension (i.e., the word sequence).
After scanning the whole input sequence, a filter generates a feature map of the input sequence length.
A 1-max pooling is applied to this feature map to obtain the most significant feature for this filter.
Due to the use of 1-max pooling, we do not need attention layer to weight word/phrase importance. 
For each window size, we use $M$=128 filters to learn complementary features from the same word windows.
That is, 1-layer CNN produces a 128-dimensional feature vector for each window size.
The feature vectors of all windows sizes are concatenated into an output feature vector for the classifier.


\begin{figure}%[h]
	\centering
	\includegraphics[scale=0.3]{cnn}
	\vspace{-3mm}
	\caption{1-layer CNN.}
	\label{fig:cnn}
	\vspace{-4mm}
\end{figure}

\begin{figure}%[h]
	\centering
	\includegraphics[scale=0.32]{lstm}
	\vspace{-4mm}
	\caption{1-layer Bi-LSTM with attention.}
	\label{fig:lstm}
	\vspace{-7mm}
\end{figure}


\textbf{2-layers CNN:}
This is a deeper variant of the 1-layer CNN.
The first CNN layer is the same as 1-layer CNN.
The three feature vectors by the first CNN  layer are fed into one more layer of convolution and 1-max-pooling.
For each input $128$-dimensional feature vector, the second layer also uses $M$ ($M=128$) filters with the window size $h=3$.
After the convolution, ReLu activation and pooling, the second CNN layer outputs three $128$-dimensional feature vectors which are concatenated into an output feature vector for the classifier.
 
 
\textbf{1-layer BiLSTM:}
Fig.~\ref{fig:lstm} shows a 1-layer BiLSTM model.
A BiLSTM model consists of a forward LSTM $\overrightarrow{lstm}_f$ network that reads the input from $w_1$ to $w_N$, and a backward LSTM $\overleftarrow{lstm}_b$ that reads from $w_N$ to $w_1$:
$\overrightarrow{h}_{i} = \overrightarrow{lstm}_f(w_i), i\in\left[1,n\right]$,
$\overleftarrow{h}_{i} = \overleftarrow{lstm}_b(w_i), i\in\left[n,1\right]$.
$\overrightarrow{h}_{i}$ and $\overleftarrow{h}_{i}$ are forward and backward hidden vectors for word $w_i$.
Both forward and backward LSTM have 192 LSTM cells.
So $\overrightarrow{h}_{i}$ and $\overleftarrow{h}_{i}$ are 192-dimensional vectors.
The parameters of $\overrightarrow{lstm}_f$ and $\overleftarrow{lstm}_f$ will be learned during model training.
We obtain the hidden vector $h_i$ for $w_i$ by concatenating $\overrightarrow{h}_{i}$ and $\overleftarrow{h}_{i}$, i.e., $h_i = \overrightarrow{h}_{i}\oplus\overleftarrow{h}_{i}$ ($h_i$ is 384-dimensional vector).
$h_i$ is the output vector of the Bi-directional LSTM for the input word $w_i$, which encodes both the preceding and succeeding sentence context centered around $w_i$.
We concatenate the last hidden vectors $\overrightarrow{h}_{N}$ and $\overleftarrow{h}_{1}$ into an output feature vector for the classifier.


\textbf{2-layers BiLSTM:}
This is a deeper variant of 1-layer BiLSTM.
The first layer is a 1-layer BiLSTM.
The output $h_i$ of the first BiLSTM layer is passed as input to the second BiLSTM layer which also uses 192 LSTM cells to encode input vector sequence.
The last hidden vectors $\overrightarrow{h}_N$ and $\overleftarrow{h}_1$ by the second layer are concatenated into an output feature vector for the classifier.


\textbf{Attention layer for BiLSTM:}
For the BiLSTM network, we can add an attention layer on top of the last BiLSTM layer to weight the importance of words.
The attention layer computes the word attention weight $\alpha_i = \exp\left(\left(u_i\right)^\mathsf{T} u_w\right)/\sum_{i}\exp\left(\left(u_i\right)^\mathsf{T} u_w\right)$ where $u_i = \tanh\left(W_w h_i + b_w\right)$.
It takes the output $h_i$ of the BiLSTM layer as input.
$h_i$ is first fed through a one-layer feed-forward neural network to get $u_i$ as a hidden representation of $h_i$.
$W_w$ and $b_w$ are learnable parameters of the attention layer.
Then, we measure the importance of the word $w_i$ as the similarity of $u_i$ with a word-level context vector $u_w$ and obtain a normalized importance weight $\alpha_i$ through a softmax function.
The vector $u_w$ is randomly initialized and learned during the training process.
Finally, we compute the output feature vector for the classifier as a weighted sum of the Bi-LSTM output vector of each word in the input sequence multiplied by the word attention weight, i.e., $\sum_{i=1}^n\alpha_i h_i$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Predicting Missing Aspects of CVEs}
\label{sec:output}

We use a softmax classifier as the output classifer. 
Given the output feature vector $F$ by the neural-network feature extractor, a softmax classifier predicts the probability distribution $\hat{y}$ over the $m$ class labels of a particular CVE aspect, i.e., $\hat{y}=softmax\left(WF + b\right)$, where $\hat{y} \in \mathcal{R}^{m}$ is the vector of prediction probabilities over the $m$ class labels, and $W$ and $b$ are the learnable parameters of the classifier.
The learnable parameters of the neural-network feature extractor and the classifier are trained to minimize the cross-entropy loss of the predicted labels and the ground-truth labels: $L\left(\hat{y},y\right)=-\sum_{i=1}^K\sum_{j=1}^m y_{ij}\log\left(\hat{y}_{ij}\right)$ where $K$ denotes the number of training samples.
$y_{ij}$ is the ground-truth label of the $j$th class (1 for the ground-truth class, otherwise 0) for the $i$th training example, and $\hat{y}_{ij}$ is the predicted probability of the $j$th class for the $i$th training example.
The loss gradient is back-propagated to update the learnable parameters of the classifier and the neural-network feature extractor.

