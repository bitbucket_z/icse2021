
\section{Experiments}

We conduct a series of experiments to investigate the following four research questions:

\begin{itemize} [leftmargin=*]
	\item \textbf{RQ1 Design of neural network classifier}: How do different input formats, word embeddings, model architectures and neural network designs affect the prediction performance?
	\zc{\textbf{Overall performance}: How well can our approach predict different CVE aspects? May merge the old RQ2 as an overall performance analysis at the end of the RQ1 results?}

	\item \textbf{RQ2 Ablation study}: What is the most and least prominent aspect or aspect combination for predicting a specific aspect?
		
	\item \textbf{\zc{RQ3 Prediction on future CVEs}}: \zc{How well can our approach predict key aspects of CVEs publised after the training CVEs?} \zc{Use CVE-specific embeddings and separate aspects in original order as input. Use two model architectures x 6 network designs as the baselines?}

	\item \textbf{RQ4 Usefulness}: Can the augmented CVE descriptions improve the severity prediction over the original CVE description?
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Experiment Setup}

We describe the CVE dataset used in our experiments, our model training setting, and the performance evaluation metrics.

\subsubsection{CVE Dataset}\label{sec:dataset}
CVE list can be downloaded from the official CVE website~\cite{cve}.
In this work, we download the CVE list that contains 120,103 CVEs from January 1999 to October 2019.
We use the aspect extraction method in Section~\ref{sec:extractionrules} to extract CVE aspects from these CVEs.
Our evaluation in Section~\ref{sec:extractionquality} confirms the high accuracy of the extracted CVE aspects.
To evaluate our prediction method, we collect 51,803 CVEs whose descriptions contain at least four aspects.
As almost all CVEs contain affected product and impact aspects (see Section~\ref{sec:extractionquality}), this means the CVEs in the dataset contain at least two of the other four aspects.
This guarantees that we have sufficient data to study aspect fusion and ablation.
For each to-be-predicted aspect (vulnerability type, root cause, attacker type or attack vector), we build an aspect-specific dataset for classifier training and testing from these 51,803 CVEs according to the method in Section~\ref{sec:approachoverview}.
Table~\ref{tab:dataset} summarizes the information of the four aspect-specific datasets.
All experiment datasets and results are available at  \href{https://github.com/pmaovr/Predicting-Missing-Aspects-of-Vulnerability-Reports}{\textcolor{blue}{the Github repository}}.
For a specific to-be-predicted aspect (e.g., vulnerability type), the other three aspects (e.g., root cause, attacker type or attack vector) of the CVEs used as model input may also be missing, except affected product and impact.
This reflects the realistic situation of CVE data.
That is, when predicting a missing aspect, it is often not all other aspects available as model input.


\begin{table}%[h]
	\centering
	\caption{Aspect-specific CVE datasets.}
	\label{tab:dataset}
	\vspace{-3mm}
	\scriptsize
	\setlength{\tabcolsep}{0.5mm}{
	\begin{tabular}{ccccc}
		\toprule
		To-be-predicted aspect&Vul-Type&Root cause&Attack vector&Attacker type\\
		Size&36,103&20,813&35,289&43,330\\
		\midrule
		\% with vulnerability type&100\%&61\%&63\%&66\%\\
		\% with root cause&38\%&100\%&39\%&37\%\\
		\% with attack vector&75\%&72\%&100\%&78\%\\
		\% with attacker type&86\%&86\%&92\%&100\%\\
		\bottomrule
	\end{tabular}}
	\vspace{-6mm}
\end{table}


\subsubsection{Model Training}
We implement the proposed neural network classifier in Tensorflow~\cite{Abadi2016TensorFlow}.
Each to-be-predicted aspect has its own classifier. 
As discussed in Section~\ref{sec:approach}, we have different choices for input format, word embedding, model architecture and network design when implementing a classifier.
All the classifiers are trained in the same setting.
Specifically, we train each model for 256 iterations with a batch size of 128, set learning rate at 0.001, and use Adam~\cite{Kingma2014Adam} as the optimizer.
All experiments run on an NVIDIA Tesla M40 GPU machine.


\subsubsection{Evaluation Metrics}\label{sec:metrics}
The multi-class classification results can be represented in a $m \times m$ confusion matrix $M$, where $m$ is the number of class labels (see Table~\ref{tab:freq}).
We use Precision (Pre), Recall (Re) and F1-score (F1) to evaluate the effectiveness of multi-class classification~\cite{mt1,mt2,mt3,mt4}.
Precision for a label $L_j$ of an aspect $A$ represents the proportion of the CVEs whose missing aspect $A$ is correctly predicted as $L_j$ among all CVEs whose missing aspect $A$ is predicted as $L_j$.
Recall for a label $L_i$ of an aspect $A$ is the proportion of the CVEs whose missing aspect $A$ is correctly predicted as $L_i$ compared with the number of ground-truth CVEs whose missing aspect $A$ is actually $L_i$.
F-score is the harmonic average of the precision and recall.
The overall performance of a classifier is the weighted average of the evaluation metrics of each class label. 
Since F1-score conveys the balance between the precision and the recall, we use F1-score as the main evaluation metric in the discussion.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Design of Neural Network Classifier (RQ1)}

\textbf{Motivation.}
The design of our neural network classifier considers three input formats (i-ao: separate CVE aspects in the original order appearing in CVE descriptions, i-ar: separate CVE aspects in random order, and i-fu: original CVE descriptions), three word embeddings (Google News, SecurityFocus versus CVE-specific), two model architecture (early aspect fusion versus late aspect fusion), and six specific neural network design (CNN versus BiLSTM, 1-layer versus 2-layer, BiLSTM with/without attention layer).
We want to investigate the impact of these design options on the prediction performance and identify the most effective design of neural network classifier.


\textbf{Approach.}
We conduct four experiments to evaluate the impact of input format, word embedding, model architecture and network design respectively.
For the experiments on one dimension, we use the most effective options for the other three dimensions.
Specifically, for input format experiments, we use CVE-specific word embeddings, early fusion architecture and 1-layer CNN.
For word embedding experiments, we use separate CVE aspects in original order, early fusion architecture and 1-layer CNN.
For model architecture experiments, we use separate CVE aspects in original order, CVE-specific word embeddings and 1-layer CNN.
For network design experiments, we use separate CVE aspects in original order, CVE-specific word embeddings and early fusion architecture.
This experiment setting helps to reduce the large number of experiments by the full Cartesian product combination of the design options, and also facilitate the analysis of each design dimension while fixing the other three dimensions.
To ensure the reliability of our experiments, we perform 10-fold cross validation in all the experiments.
For each fold, we use 80\%, 10\% and 10\% of data for model training, hyperparameter optimization and testing respectively.
We conduct Wilcoxon signed-rank test~\cite{woolson2007wilcoxon} on F1-score between different experiment settings.
$p$-value $<0.05$ is considered statistically significant (marked by * in the results tables).


\begin{table}%[h]
	\centering
	\scriptsize
	\caption{\label{tab:inputformats} Impact of input formats.}
	\vspace{-3mm}
	\setlength{\tabcolsep}{0.5mm}{
		\begin{tabular}{cccccccc}
			\toprule
			& & & Vul-Type& Root Casue& Attack Vector &Attacker Type\\
			\midrule	
			\multirow{3}{*}{Pre}
			&&i-ao & 0.945& 0.779 & 0.708 & 0.884\\
			&&i-ar& 0.943& 0.746& 0.701 & 0.882\\
			&&i-fu  & 0.946 & 0.783 & 0.703&0.888 \\
			\midrule
			\multirow{3}{*}{Re}
			&&i-ao& 0.945& 0.793& 0.716 & 0.897\\
			&&i-ar& 0.945& 0.770& 0.710& 0.892\\
			&&i-fu& 0.946& 0.796 & 0.717 &0.899\\
			\midrule	
			\multirow{3}{*}{F1}
			&&i-ao & 0.943& 0.780& 0.704 &0.885\\
			&&i-ar & 0.943& 0.745& 0.699 &0.880\\
			&&i-fu& 0.946& 0.788 & 0.706 &0.889\\
			\bottomrule
		\end{tabular}
	}
	\vspace{-4mm}
\end{table}


\begin{table}%[h]
	\centering
	\scriptsize
	\caption{\label{tab:wordembedding}Impact of word embeddings.}
	\vspace{-3mm}
	\setlength{\tabcolsep}{0.5mm}{
		\begin{tabular}{llccccc}
			\toprule
			& & & Vul-Type& Root Casue& Attack Vector &Attacker Type\\
			\midrule	
			\multirow{3}{*}{Pre}&&CVE& 0.946  & 0.783& 0.703&0.888 \\
			&&SecurityFocus& 0.942  & 0.779& 0.701& 0.883\\
			&&*Google news& 0.932 & 0.759& 0.687 & 0.869\\
			\midrule
			\multirow{3}{*}{Re}&&CVE& 0.946 & 0.796& 0.717 &0.899\\
			&&SecurityFocus& 0.944 & 0.792& 0.716  &0.894\\
			&&*Google news& 0.935  & 0.784& 0.688& 0.885\\
			\midrule	
			\multirow{3}{*}{F1}&&CVE& 0.946 & 0.788& 0.706 &0.889\\
			&&SecurityFocus& 0.942 & 0.783& 0.703  &0.883\\
			&&*Google news& 0.933& 0.761 & 0.687 & 0.871\\
			\bottomrule
		\end{tabular}
	}
	\vspace{-7mm}
\end{table}


\begin{table}%[h]
	\centering
	\scriptsize
	\vspace{-7mm}
	\caption{\label{tab:modelarchitecture}Impact of model architectures.}
	\vspace{-3mm}
	\setlength{\tabcolsep}{0.5mm}{
		\begin{tabular}{llccccc}
			\toprule
			& & & Vul-Type& Root Casue& Attack Vector &Attacker Type\\
			\midrule	
			\multirow{3}{*}{Pre}&&Early Fusion& 0.946 & 0.783& 0.703 &0.888 \\
			&&*Late Fusion& 0.921 & 0.751& 0.671 & 0.844\\
			\midrule
			\multirow{3}{*}{Re}&&Early Fusion& 0.946& 0.796 & 0.717 &0.899\\
			&&*Late Fusion& 0.927 & 0.770& 0.680  &0.872\\
			\midrule	
			\multirow{3}{*}{F1}&&Early Fusion& 0.946 & 0.788& 0.706 &0.889\\
			&&*Late Fusion & 0.923  & 0.755 & 0.669&0.850\\
			\bottomrule
		\end{tabular}
	}
	\vspace{-5mm}
\end{table}


\begin{table}%[htbp!]
	\centering
	\scriptsize
	\caption{\label{tab:detail}Impact of neural network designs.}
	\vspace{-3mm}	
	\setlength{\tabcolsep}{0.5mm}{\begin{tabular}{cccccccc}
		\toprule
		& & & Vul-Type& Root Casue& Attack Vector &Attacker Type\\
		\midrule
		\multirow{7}{*}{Pre}&&1-L CNN  & 0.946& 0.783 & 0.703 &0.888 \\
		&&2-L CNN  & 0.933 & 0.765& 0.673 & 0.852\\
		&& 1-L BiLSTM      &  0.939 & 0.761& 0.682& 0.867\\
		&&2-L BiLSTM               & 0.939 & 0.770&0.688 & 0.870\\	
		&&1-L BiLSTM+Attention      &  0.941 & 0.769& 0.690& 0.873\\
		&&2-L BiLSTM+Attention   &  0.943 & 0.778& 0.692&0.876\\
		\midrule
		\multirow{7}{*}{Re} &&1-L CNN             & 0.946 & 0.796& 0.717 &0.899\\
		&&2-L CNN             & 0.935  & 0.775& 0.701& 0.878 \\
		&&1-L BiLSTM      &  0.938 & 0.778&0.706& 0.882\\
		&&2-L BiLSTM               & 0.941 & 0.780&0.703 & 0.883\\
		&&1-L BiLSTM+Attention             & 0.943&0.778 &0.713& 0.887 \\
		&&2-L BiLSTM+Attention                & 0.945& 0.792 &0.714 & 0.889\\
		\midrule
		\multirow{7}{*}{F1}  &&1-L CNN             & 0.946 & 0.788& 0.706&0.889\\
		&&2-L CNN             & 0.932 & 0.768 & 0.677& 0.859 \\
		&&1-L BiLSTM      &  0.938 & 0.765& 0.684& 0.871\\
		&&2-L BiLSTM               & 0.940 & 0.770&0.683 & 0.874\\
		&&1-L BiLSTM+Attention            & 0.940& 0.770 & 0.692 & 0.873\\
		&&2-L BiLSTM+Attention             & 0.943& 0.778 & 0.694&0.878\\
		\bottomrule
	\end{tabular}}
	\vspace{-7mm}
\end{table}


\textbf{Results.}

\subsubsection{Input Formats}
Table~\ref{tab:inputformats} presents the results.
We can see that the three input formats do not statistically significantly affect the prediction of the four CVE aspects, with only 0.002-0.009 difference in F1 across the three input formats.
The only exception is the prediction of root cause by separate aspects in random order (i-ar), but the difference in F1 is not very large either.
This suggests that the phrase-level information in the CVE aspects alone can support reliable prediction. 
The presence or absence of the additional information (mostly prepositions, pronouns, determiner) that connect CVE aspects in the original CVE descriptions does not significantly affect the prediction.
Furthermore, the prediction is not sensitive to the appearance order of different aspects in the CVE descriptions.
Therefore, we use separate aspects in the original appearance order (i-ao) as the default option.


\subsubsection{General versus Domain-Specific Word Embeddings}
Table~\ref{tab:wordembedding} shows that domain-specific word embeddings (CVE and SecurityFocus) support more accurate prediction in all four aspects than general word embeddings (Google News).
The differences are statistically significant in F1.
However, the two domain-specific word embeddings have marginal differences.
This result can be attributed to two reasons.
First, domain-specific word embeddings learn meaningful embeddings for domain-specific terms (e.g., CRLF, XSS, DoS), which may be regarded as out-of-vocabulary words in general word embeddings.
Second, domain-specific corpus allows the learning of ``purer'' word embeddings highly relevant to a particular domain, while the word embeddings learned from general text may embed some unnecessary  ``noise'' irrelevant to the particular domain.
In this work, we use CVE-specific word embeddings as the default option.


\subsubsection{Early Fusion versus Late Fusion}\label{sec:aspectfusion}
Table~\ref{tab:modelarchitecture} shows that early fusion architecture performs better than late fusion architecture (statistically significant for all evaluation metrics).
This suggests that using a single network to extract and fuse features directly from all input CVE aspects is much more effective than extracting features from each CVE aspect separately and only fusing the features of different aspects at the end.
Therefore, we use early aspect fusion as the default option.


\subsubsection{Neural Network Variants}
Table~\ref{tab:detail} present our experimental results on the six variants of neural network feature extractor.
We can see that 1-layer CNN outperforms the other five variants.
So we use 1-layer CNN as the baseline to analyze the performance of the other five variants.
Compared with 1-layer CNN, 2-layer CNN has worse but statistically non-significant performance for predicting vulnerability type and attacker vector, but has statistically significant worse performance for predicting root cause and attacker type.
This suggests that deeper CNN is less appropriate than 1-layer CNN in our text classification task.
The performance of the four BiLSTM networks are very close.
Neither deeper BiLSTM nor attention mechanism statistically significantly improve the prediction performance.
1-layer CNN has better performance than 2-layer BiLSTM with attention (the overall best BiLSTM performer).
Although the performance differences are not large, the differences in F1 are statistically significant for predicting all four CVE aspects.
This result suggests that using CNN to extract important words/phrase features fits better for our text classification task than using LSTM to learn long-range sentence features.


\noindent
\fbox{\begin{minipage}{8.4cm} 
		\emph{According to our experiments on input formats, word embeddings, model architectures and network designs, the most effective design of the classifier takes as input separate CVE aspects in original order, uses CVE-specific word embeddings to represent input text, and adopts early-fusion architecture and 1-layer CNN as feature extractor.} 
	\end{minipage}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Overall Performance (RQ2)}

\textbf{Motivation.}
In this work, we predict four aspects: vulnerability type, root cause, attacker type and attack vector, which suffer from different severities of information missing in CVE descriptions (see Section~\ref{sec:extractionquality}).
Although vulnerability type is a compulsory aspect to enter when submitting the new CVE request, 56\% of CVE descriptions do not provide vulnerability type by leaving it as Other or Unknown.
Root cause, attacker type and attack vector are optional.
About 85\%, 28\% and 38\% of CVEs do not provide root cause, attacker type and attack vector, respectively.
We want to see how well our approach can augment these missing aspects for CVEs.


\textbf{Approach.} 
We study the prediction performance of the most effective classifier design identified in RQ1.


\textbf{Results.}
The precision, recall and F1 results can be found in the 1-layer CNN rows in Table~\ref{tab:detail}.
Our approach achieves 0.946 in F1 for predicting vulnerability type.
It also achieves very high F1 (0.889) for predicting attacker type.
As discussed in Section~\ref{sec:preliminary}, vulnerability type and attacker type are clearly defined categorical values.
Each class label has a distinct semantic.
This makes it easier to make accurate prediction on vulnerability type and attacker type.


In contrast, the prediction accuracy for root cause is worse. 
Compared with the distinct vulnerability types and attacker types, the error classes of root cause are relatively less distinguishable, for example, configuration versus environment, boundary condition versus input validation, access validation versus origin validation.
The fuzziness of these error classes makes it more challenging to accurately predict root cause.
Another challenge for predicting root cause is that there are six classes with only a very small number of instances, for example, atomicity error, race condition error (see Table~\ref{tab:freq}).
This is referred to as few-shot learning~\cite{wang2019few} which affects the model performance.


Our approach does not perform very well for attack vector (only 0.706 in F1).
This is because attack vector is often highly related to the information of some specific software products or vulnerabilities.
This is make it difficult to generalize the prediction model from training CVEs to unseen CVEs.


\noindent
\fbox{\begin{minipage}{8.4cm} 
		\emph{Our approach can accurately augment the missing vulnerability types and attacker types. However, it has limitations to predict fuzzy root causes and product- or vulnerability-specific attack vectors.} 
	\end{minipage}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Results: Ablation Study (RQ2)}
\label{sec:aspectablation}

\begin{table}
	\scriptsize
	\centering
	\caption{\label{tab:reduce_vt}Ablation results for predicting vulnerability type.}
	\vspace{-3mm}
	\setlength{\tabcolsep}{0.5mm}{
		\begin{tabular}{ccccccc}
			\toprule
			&Ablated aspect&Root cause&Affected product& Impact&Attacker type &Attack vector\\
			\midrule
			&Pre & 0.943    & 0.925&  0.821 &0.939 & 0.888\\
			&Re & 0.943 & 0.927& 0.822  &0.941& 0.896  \\
			&F1  & 0.943& 0.925& 0.821 &0.939 &0.890\\
			\bottomrule
		\end{tabular}
	}
	\vspace{-4mm}
\end{table}

\begin{table}
	\scriptsize
	\centering
	\caption{\label{tab:reduce_rc}Ablation results for predicting root cause.}
	\vspace{-3mm}
	\setlength{\tabcolsep}{0.5mm}{
		\begin{tabular}{ccccccc}
			\toprule
			&Ablated aspects& Vul-Type&Affected product & Impact&Attacker type  &Attack vector\\
			\midrule
			&Pre  & 0.740 &0.734&  0.739 &0.781&0.780\\
			&Re  & 0.751 &0.741 & 0.755&0.793&0.795  \\
			&F1  & 0.745& 0.730& 0.736  &0.785&0.784\\
			\bottomrule
		\end{tabular}
	}
	\vspace{-7mm}
\end{table}

\begin{table}
	\scriptsize
	\centering
	\vspace{-7mm}
	\caption{\label{tab:reduce_at}Ablation results for predicting attacker type.}
	\vspace{-3mm}
	\setlength{\tabcolsep}{0.5mm}{
		\begin{tabular}{ccccccc}
			\toprule
			&Ablated aspect & Vul-Type & Root cause&Affected product& Impact&Attack vector \\
			\midrule
			&Pre    &  0.852&0.873  &0.850& 0.883& 0.864  \\
			&Re     & 0.876 &0.892 &0.874& 0.895 &0.871  \\
			&F1  & 0.861&0.878&0.847  & 0.881 & 0.863 \\
			\bottomrule
		\end{tabular}
	}
	\vspace{-4mm}
\end{table}

\begin{table}
	\scriptsize
	\centering
	\caption{\label{tab:reduce_av}Ablation results for predicting attack vector.}
	\vspace{-3mm}
	\setlength{\tabcolsep}{0.5mm}{
	\begin{tabular}{ccccccc}
			\toprule
			&Ablated aspect & Vul-Type& Root cause &Affected product& Impact&Attacker type \\
			\midrule
			&Precision    &  0.659&0.696 & 0.568 & 0.680 &0.670\\
			&Recall     & 0.693 &0.701&0.601 & 0.700 &0.674 \\
			&F1  & 0.665  &0.695& 0.572 & 0.683&0.669\\
			\bottomrule
		\end{tabular}
	}
	\vspace{-4mm}
\end{table}


\textbf{Motivation.}
As shown in Table~\ref{tab:dataset}, it is unrealistic to assume that all other five CVE aspects are available for predicting a particular aspect.
In this RQ, we want to investigate the impact of certain CVE aspects unavailable as input on the accuracy of predicting a particular aspect.
This study has two important roles.
First, it identifies stronger correlations (if any) among some CVE aspects than others.
Second, it identifies the minimum subset of known aspects required for making reliable prediction of a particular aspect. 
This also help us to understand the practicality of our approach.


\textbf{Approach.}
For a particular aspect, we conduct five experiments.
In each experiment, we ablate one of the other five aspects in the aspect-specific dataset, which produces an ablation dataset without the ablated aspect.
For example, for vulnerability type, we obtain five datasets without root cause, affected product, impact, attacker type or attack vector for the five ablation experiments, respectively.
Although the experiments in RQ1/RQ2/RQ3 do not assume the availability of all five aspects, the ablation of one aspect in this RQ means that we completely ignore this ablated aspect as known aspect for model input, even the CVEs describe this ablated aspect.
We use the most effective classifier design identified in RQ1, and train and test the classifier on each ablation dataset.
We perform 10-fold cross-validation in all experiments.
 

\textbf{Results.}
Table~\ref{tab:reduce_vt}-Table~\ref{tab:reduce_av} show our experimental results. 
We compare the performance metrics with those by 1-layer CNN in Table~\ref{tab:detail}.
For predicting vulnerability type, ablating impact results in the most significant drop (12.6\%) in F1, followed by ablating attack vector (5.6\% drop in F1).
In contrast, ablating attacker type and affected product have a much less significant impact, with 0.7\% and 2.1\% drop in F1 respectively. 
Ablating root cause almost has no impact on predicting vulnerability type.
As discussed in Section~\ref{sec:extractionquality}, over 94\% of CVEs describe impact aspect.
Therefore, our approach will not actually suffer from the performance degradation due to the unavailability of impact as input in practice.
Although unavailable attack vector as input aspect affects the prediction of vulnerability type, our approach can still achieve high accuracy (0.89 in F1).


For predicting attack vector, ablating affected product has the most significant impact, resulting in 13.4\% drop in F1.
However, as almost all CVEs describe affected product, our approach will not actually suffer from the performance degradation due to the unavailability of affected product as input.
Ablating the other four aspects results in much smaller drop (about 1.1\%-4.1\%) in F1.
Among these four aspects, vulnerability type and attacker type have stronger correlations with attack vector than impact and root cause.


There are no such a prominent aspect for predicting root cause and attacker type as impact for vulnerability type and affected product for attack vector.
For predicting root cause, ablating vulnerability type, affected product or impact has relatively larger impact (about 5\% drop in F1), compared with about 0.4\% drop in F1 by ablating attacker type and attack vector.
For predicting attacker type, ablating affected product results in relative larger drop in F1 (4.2\%) than ablating the other four aspects.
Again, we do not have the real issue of unavailable affected product and impact as input in practice.


\noindent
\fbox{\begin{minipage}{8.4cm} 
		\emph{Our ablation study identifies two strong correlations: vulnerability type and impact, affected product and attack vector. Although the unavailability of certain aspect(s) in the CVE description degrades the prediction performance of an unknown aspect, it does not significantly affect the practicality of our approach.}
	 \end{minipage}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Results: Prediction on Future CVEs (RQ3)}
\label{sec:futureRQ3}

\begin{table}%[htbp!]
	\centering
	\scriptsize
	\caption{Prediction on future CVEs results.}
	\vspace{-3mm}
	\setlength{\tabcolsep}{0.5mm}{\begin{tabular}{cccccccc}
		\toprule
		& & & Vul-Type& Root Casue& Attack Vector &Attacker Type\\
		\midrule
		\multirow{7}{*}{Pre}&&1-L CNN  & 0.891& 0.717 &0.621 & 0.820 \\
		&&2-L CNN  & 0.881 & 0.711& 0.600 & 0.802\\
		&&1-L BiLSTM      &  0.880 & 0.712& 0.605& 0.809\\
		&&2-L BiLSTM               & 0.857 & 0.711&0.609 & 0.805\\	
		&&1-L BiLSTM+Attention      & 0.861 & 0.714& 0.610& 0.813\\
		&&2-L BiLSTM+Attention   &  0.873 & 0.715& 0.615&0.816\\
		\midrule
		\multirow{7}{*}{Re} &&1-L CNN             & 0.889& 0.717  &0.623& 0.825 \\
		&&2-L CNN             & 0.880  & 0.710& 0.603& 0.803\\
		&&1-L BiLSTM      &  0.861& 0.710&0.610& 0.809\\
		&&2-L BiLSTM               & 0.863 & 0.700&0.611 & 0.809\\
		&&1-L BiLSTM+Attention             & 0.867&0.715 &0.619& 0.817 \\
		&&2-L BiLSTM+Attention                & 0.870& 0.713 &0.619& 0.818\\
		\midrule
		\multirow{7}{*}{F1}  &&1-L CNN             & 0.880& 0.714 &0.611 &0.814\\
		&&2-L CNN             & 0.850 & 0.702 & 0.589& 0.780 \\
		&&1-L BiLSTM      &  0.857 & 0.705& 0.596& 0.792\\
		&&2-L BiLSTM               & 0.859 & 0.702&0.599 & 0.799\\
		&&1-L BiLSTM+Attention            & 0.862& 0.710 & 0.605 & 0.815\\
		&&2-L BiLSTM+Attention             & 0.869& 0.711 & 0.607&0.812\\
		\bottomrule
	\end{tabular}}
	\label{tab:future} 
	\vspace{-4mm}
\end{table}


\textbf{Motivation.}
Software vulnerabilities are constantly being discovered and reported, and we want to see if our model training can learn the relationship between CVE aspects from ``historical'' data sets and be well applied to future data.


\textbf{Approach.}
To confirm the effectiveness of our aspect augmentation approach, we have constructed a "future" data set of 8,220 CVEs (from October 2016 to December 2019).
After that, we train a model with historical data as the input of neural network, and then use future data as the input in the prediction stage to predict and learn the accuracy of future data from historical data.


\textbf{Results.}
Table~\ref{tab:future} Through the training of historical data sets, our method can predict the missing vulnerability type, root cause, attacker type and attack vector of future data sets with accuracy of 88\%, 71\%, 61\% and 81\% respectively.


\noindent
\fbox{\begin{minipage}{8.4cm} 
		\emph{Due to the development of software, the future data type can be predicted by historical data, but the overall performance degradation is still within an acceptable range.}
	\end{minipage}
}

\zc{add new experiments ...}

\zc{Use CVE-specific embeddings and separate aspects in original order as input. Use two model architectures x 6 network designs as the baselines?}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Results: Usefulness (RQ4)}
\label{sec:futureRQ4}

\begin{table}
	\scriptsize
	\centering
	\caption{Completion and ablation for severity prediction results.}
	\vspace{-3mm}
	\setlength{\tabcolsep}{0.5mm}{
		\begin{tabular}{ccccccccc}
			\toprule
			&Ablated & \multirow{2}{*}{Completion} &Original & \multirow{2}{*}{Vul-Type}  &\multirow{2}{*}{Root cause} & \multirow{2}{*}{Attack vector} & \multirow{2}{*}{Attack type} \\
			&aspect & & CVE & & & & \\
			\midrule	
			&Pre&0.831&0.807& 0.749& 0.792  & 0.751&0.760\\
			&Re &0.831&0.805& 0.749& 0.789& 0.749  &0.764  \\	
			&F1  &0.830&0.805& 0.746& 0.785  &0.746&0.763\\
			\bottomrule
		\end{tabular}
	}
	\vspace{-6mm}
	\label{tab:severity}
\end{table}


\textbf{Motivation.}
It is assumed that the absence of a CVE aspect is influential in predicting vulnerability severity levels.
In this RQ, we want to know how the missing of vulnerability information affects the prediction of vulnerability severity levels.
The following information can be obtained from this study.
First, it determines the impact of vulnerability information completion on the result of vulnerability severity level prediction.
Secondly, it determines the impact of the absence of vulnerability information on the result of vulnerability severity prediction.
At the same time, it helps us validate the contribution our approach can make to other vulnerability research.


\textbf{Approach.}
For prediction of CVE severity, we use seven experiments, which are still use in the ablation study (see Section~\ref{sec:aspectablation}).
We add the information predicted by our model into the vulnerability description to predict the severity level of the vulnerability.
Then, we used the complete CVE description to compare with the description with missing CVE aspect and original CVE description.
We obtained six data sets containing no vulnerability type, no root cause, no attacker type or attack vector ,original CVE description and one data set containing all vulnerability information.
By comparing the relatively missing data set with the experiment of the data set containing all the data, we can find out the influence of the missing information on the work of predicting the severity of the vulnerability.
We perform 10-fold cross-validation in all experiments.
 

\textbf{Results.}
Table~\ref{tab:severity} show our experimental results. 
We can see that the vulnerability information completion can improve the prediction of vulnerability severity level, up about 2.5\%.
We compare the performance metrics with those by 1-layer CNN in Table~\ref{tab:detail}.
For predicting vunnerability severity, ablating attack vector and vul-type have the most significant impact, resulting in  5.9\% drop in F1.
Ablating the four aspects results in relatively smaller drop (about 2.0\%-5.9\%) in F1.


\noindent
\fbox{\begin{minipage}{8.4cm} 
		\emph{Our study found that the missing of vulnerability information had a relatively large impact on the prediction of vulnerability severity level, and to some extent, it verified the practicability of our method.}
	\end{minipage}
}

