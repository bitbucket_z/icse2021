\vspace{-2mm}
\section{Introduction}
\vspace{-1mm}

Software vulnerabilities can be exploited to damage system or information confidentiality, integrity and availability~\cite{cvss}.
Significant efforts have been made to document and manage publicly known software vulnerabilities.
At the core of these efforts is the Common Vulnerabilities and Exposures (CVE)~\cite{cve}.
CVE is a list of entries - each reporting a publicly known vulnerability with an unique identification number, a
description, and at least one public reference of the initial announcement of the vulnerability.
At the time of this work, over 137,000 vulnerabilities were indexed in the CVE database.
With the fast growth of massive CVE data, there is also a growing concern about the information quality~\cite{mu2018vul,dong2019towards}.


\begin{figure}[htbp]
	\vspace{-3mm}
	\centering
	
	\includegraphics[scale=0.5]{CVEview}
	\vspace{-2mm}
	
	\caption{An example of CVE description.}
	\vspace{-3mm}
	\label{fig:cvedesccomplete}	
\end{figure}

\begin{figure}[htbp]
	\centering
	%\includegraphics[width=\linewidth]{missinfor}
	\subfigtopskip=0pt %设置子图与上面正文或别的内容的距离
	\subfigbottomskip=0pt %设置第二行子图与第一行子图的距离，即下面的头与上面的脚的距离
	\subfigcapskip=-5pt
	
    \subfigure[CVE-2019-5934: missing Root Cause]{\includegraphics[scale=0.460]{CVE-2019-5934}}
    \subfigure[CVE-2019-13561: missing Root Cause and Vulnerability Type]{\includegraphics[scale=0.460]{CVE-2019-13561}}
    \subfigure[CVE-2019-15117: missing Vulnerability Type]{\includegraphics[scale=0.460]{CVE-2019-15117}}
    \subfigure[CVE-2019-14262: only have Affected Product and Impact]{\includegraphics[scale=0.460]{CVE-2019-14262}}
	%\setlength{\belowcaptionskip}{-0.2cm}
		
	\caption{Examples of CVEs that miss information.}
	\vspace{-7mm}
	\label{fig:cvedescmissing}	
%\vspace{-3mm}
\end{figure}


In this work, we are concerned with the \textit{information completeness of the CVE descriptions}.
Fig.~\ref{fig:cvedesccomplete} is the description of CVE entry CVE-2005-4676.
As highlighted in Fig.~\ref{fig:cvedesccomplete}, a high-quality CVE description should describe six key aspects of vulnerability~\cite{poj}, 
including \textit{vulnerability type} (e.g., buffer overflow), 
\textit{affected product} (including vendor/version/component information, e.g., Andreas Huggel Exiv2 before 0.9), 
\textit{root cause} (e.g., does not null terminate strings before calling sscanf), \textit{attacker type} (e.g., remote attacker), 
\textit{impact} (e.g., cause a denial of service - application crash), 
and \textit{attack vector} (e.g., via images with crafted IPTC metadata).


Describing these key aspects of CVEs is crucial for effective vulnerability management, mitigation and prevention.
CVE descriptions are indicative for understanding and assessing the severity~\cite{han2017Learning}, exploitability~\cite{booo}, and many other characteristics (e.g., compromise of system confidentiality, integrity and availability)~\cite{gong2019joint} of the vulnerabilities.
CVE descriptions are also an important information source for inferring the related library names of a CVE in the context of software composition analysis~\cite{yang2020vul}.
Vulnerabilities are often documented in multiple databases, such as the CVE~\cite{cve} curated by ``the power of the crowd'' and the National Vulnerability Database (NVD)~\cite{nvd} established by the US government (i.e., NIST~\cite{nist}).
CVE descriptions are the starting point to detecting the inconsistencies between different vulnerability databases~\cite{dong2019towards}.
Last but not least, CVE descriptions are the foundation for establishing the traceability links between vulnerabilities, exploits and patches, for example, the CVE-2018-2628 in the CVE, the exploit method 44,553 in the ExploitDB, and the patch commit in the Github repository.
Such traceability links are crucial for localizing vulnerable functions in the source code~\cite{zhou2019devign} and for developing and deploying patches~\cite{whatsapprcepatched,cve20205260}. 


Considering the importance of CVE descriptions, \textit{our first goal is to systematically understand the information completeness of the CVE descriptions}.
To that end, we develop rule based method to extract the six key aspects, including affected product, vulnerability type, root cause, impact, attacker type and attack vector, from the CVE descriptions.
Failure to extract certain aspect suggests the missing of that aspect in the CVE description. 
To develop aspect extraction rules, we first randomly sample 20\% CVEs per year from 1999 to 2019. 
We manually extract the aspects present in the description of these sampled CVEs.
Based on the extracted aspects, we develop regular expression patterns for aspect extraction.
We apply our aspect extraction rules to the 120,103 CVEs published from January 1999 to October 2019.
Considering the large number of the extracted CVE aspects, we adopt a sampling method~\cite{samp} to estimate the accuracy of the extracted aspects.
The extracted CVE aspects are of high quality ($\textgreater$97\% accuracy at the 95\% confidence level and 5\% error margin).


Based on the extracted aspects, we inspect the presence or absence of the six key aspects in the description of these 120,103 CVEs.
We find that almost all (over 99\%) of CVEs describe affected product.
In fact, when submitting a new CVE request, the reporter must specify its affected product.
However, all other five key aspects can be left unspecified (see in Fig.~\ref{fig:cvedescmissing}).
Among the other five key aspects, 94\% of CVEs describe the impact.
Impact describes what the attacker gains by exploiting this vulnerability, which is relatively easy to observe.
Compared with the impact, much more CVEs miss the other four more technical aspects - vulnerability type, root cause, attacker type and attack vector.
According to the guideline of CVE key details~\cite{poj}, a CVE should describe either vulnerability type or root cause, but not necessarily both.
We find that only 58\% of CVEs describe either vulnerability type or root cause, and a small 3\% of CVEs describe both.
But the rest 42\% describe neither of them.
Furthermore, 28\% of CVEs miss attacker type, and 38\% of CVEs miss attack vector.


In face of the significant missing of vulnerability type, root cause, attacker type and attack vector information in CVE descriptions, \textit{our second goal is to augment these missing aspects}.
Unfortunately, rule-based approach will not work for this augmentation task, because the correlations among different vulnerability aspects and their combinations are intricate, which cannot be easily expressed as a set of explicit rules (see the experiment results of aspect fusion and aspect ablation in Section~\ref{sec:aspectfusion} and Section~\ref{sec:aspectablation} respectively).
In this work, we adopt a neural-network based method which can learn the intricate correlations among different vulnerability aspects directly from the existing CVE description, and thus removes the need of manual feature engineering.
We formulate this task as a multi-class text classification task - predict the label of certain missing aspect of a vulnerability based on its known aspects.
We systematically explore the design space of the neural-network based classifier, including input text format and representation, model architecture and network design.


We build a ``historical'' dataset of 40,583 CVEs (till Sep. 2016) which contains at least four of the six CVE aspects. 
Specifically, we consider one aspect as the ``missing'' aspect and the rest as ``known'' aspects for prediciton.
We conduct extensive experiments to study the impact of different design choices of the neural-network classifier on the prediction performance.
In the 10-fold cross validation experiments on the historical dataset, the best classifier design achieves the prediction accuracy 94\%, 79\%, 89\% and 70\% for vulnerability type, root cause, attacker type and attack vector, respectively.
We also set ablation experiments to determine the most and least prominent aspect or aspect combinations for predicting a particular missing aspect.
Our results show that the impact aspect has the greatest impact on the prediction of vulnerability type, while affected product and vulnerability type have the greatest impact on the prediction of root cause, attacker type and attack vector.
At the same time, root cause and attacker type have least impact on the prediction of other aspects.


To confirm the effectiveness of our aspect augmentation method, we build a ``future'' dataset of 8,220 CVEs (from Oct. 2016 to Dec. 2019).
Trained on the historical dataset, our method achieves the prediction accuracy 88\%, 71\%, 61\% and 81\% for predicting the missing vulnerability type, root cause, attacker type and attack vector on the future dataset, respectively.
To demonstrate the usefulness of the augmented CVE description, we predict the CVE severity using the approach proposed by Han et al.~\cite{han2017Learning} with the original CVE descriptions and the augmented CVE descriptions, respectively.
Our results show that the augmented CVE descriptions results in 2.5\% higher prediction accuracy than the original CVE descriptions.


This paper makes the following contributions:
\begin{itemize}%[leftmargin=*]
	\item Our work is the first to investigate the aspect missing issue of the CVE entries. We analyze 24,042 CVEs over the 20 years to develop rules for extracting six CVE aspects from the CVE descriptions. We further analyze the severity and characteristics of different CVE aspects.
	\item We design a machine-learning approach for predicting the missing information of key aspects of CVEs based on the known aspects in the CVE descriptions. Our machine-learning model design systematically considers variations in input formats, word embeddings, model architectures and neural network designs.
	\item We conduct large-scale experiments to compare the effectiveness of different model design variants, investigate the prediction performance and the minimum effective amount of training data, identify the prominent correlations among different aspects for reliable prediction, and confirm the usefulness of our augmented CVE descriptions when predicting ``future'' CVEs.
	
\end{itemize}




 



