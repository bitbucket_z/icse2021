\vspace{-1mm}
\section{Detecting Missing Key Aspects}
\label{sec:aspectextraction}
\vspace{-1mm}

In this section, we introduce the six key aspects for CVE descriptions, discuss how to extract these key aspects, evaluate the quality of the extracted key aspects, and analyze the missing of different aspects in CVE descriptions.

\subsection{Preliminaries of CVE Key Aspects}
\label{sec:preliminary}

CVE suggests two description templates~\cite{poj}:
1) [Vulnerability Type] in [Component] in [Vendor][Product][Version] allows [Attacker Type] to [Impact] via [Attack Vector];
2) [Component] in [Vendor][Product][Version][Root Cause], which allows [Attacker Type] to [Impact] via [Attack Vector].
These two templates identify six key aspects for describing CVEs, as explained below.


\textbf{Vulnerability type (Vul-Type)} identifies an abstract software weakness for a CVE, which is usually identified as an entry in Common Weakness Enumeration (CWE)~\cite{cwe}.
When submitting a new CVE request~\cite{https://cveform.mitre.org/}, the reporter must specify the vulnerability type.
The request site provides several common candidate software weaknesses for selection (see in Table~\label{tab:freq}).
If the relevant weakness (e.g., PHP Remote File Inclusion (CWE-98)) is not in this list, the reporter can select Other or Unknown, and may optionally mention the weakness in the description.


\textbf{Root cause} is an error in program design, value or condition validation, and system or environment configuration, which results in a CVE.
SecurityFocus~\cite{sf} abstracts the root causes of CVEs into 11 error classes (see in Table~\label{tab:freq}).
But when submitting a new CVE request, specifying root cause is not enforced.
The reporter may describe the root cause in free-form text, as shown in Fig.~\ref{fig:cvedesccomplete} and Fig.~\ref{fig:cvedescmissing}.


\textbf{Affected product} refers to [Component] in [Vendor][Product] [Version] information in the CVE description. 
It identifies software component in certain version(s) of a software product that has been affected by a CVE.
As the examples in Fig.~\ref{fig:cvedesccomplete} and Fig.~\ref{fig:cvedescmissing} shown, affected components can be source code file, function, or executable.
When submitting a new CVE request, the reporter must provide affected product(s) and version(s), and product vendor(s).


\textbf{Attacker type} describes the mechanism by which an attacker may exploit a CVE.
The CVE request site provides five mechanisms for selection: 
authenticated, local, remote, physical, context dependent.
Attacker type is an optional field.
That is, the reporters leave this field unspecified or select other.
But they may mention attacker type in the CVE description (see Fig.~\ref{fig:cvedesccomplete} for an example).


\textbf{Impact} indicates what the attacker gains by exploiting this vulnerability.
The CVE request site provides four common impacts for selection:
code execution, information disclosure, denial of service, escalation of privileges. 
Impact is also an optional field, which can be left unspecified.
But the reporter generally describes the impact in the CVE description (see Fig.~\ref{fig:cvedesccomplete} and Fig.~\ref{fig:cvedescmissing} for examples).


\textbf{Attack vector} describes the method of exploitation, for example, to exploit vulnerability, someone must open a crafted JPEG file.
Specifying attack vector is not enforced.
It may be mentioned in the CVE description (see Fig.~\ref{fig:cvedesccomplete} and Fig.~\ref{fig:cvedescmissing} for examples).
We manually label attack vector descriptions into five common types: via field, arguments or parameters, via some crafted data, by executing the script, HTTP protocol correlation, call API.


\noindent
\fbox{
	\begin{minipage}{8.4cm} 
		\emph{When submitting a new CVE request, the reporter provides a free-form textual description of the vulnerability, which may or may not cover all the six key aspects.
		The submission form provides pre-defined options for vulnerability type, attacker type and impact. 
		But the reporter may select Other if the pre-defined options are not appropriate for the reported vulnerability or leave the options unspecified.
		As such, not all CVEs describe all six aspects (see in  Fig.~\ref{fig:cvedescmissing}).} 
	\end{minipage}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Aspect Detection in CVE Descriptions}
\label{sec:extractionrules}

To understand the missing of the six key aspects in CVE descriptions, we need to extract these aspects from the description text.  
To that end, we randomly sample 20\% of the CVEs per year from 1999 to 2019 (in total 24,042 CVEs), and manually label the key aspects in the CVE descriptions. 
We observe that 71\% of CVE descriptions follow the suggested templates~\cite{poj}, such as those in Fig.~\ref{fig:cvedesccomplete} and Fig.~\ref{fig:cvedescmissing}(a)(b).
29\% of CVE descriptions do not follow the suggested templates, such as those in Fig.~\ref{fig:cvedescmissing}(c)(d).
However, even for those non-template-following CVE descriptions, the descriptions of CVE aspects still exhibit similar patterns.
Due to the input assistance of the CVE request website, we observe commonly used phrases or their variants for vulnerability type, attacker type and impact.


Based on our observation of the aspect-phrase and sentence patterns in CVE descriptions, we develop a set of regular expression patterns to extract the six key aspects from CVE descriptions (see in \href{https://github.com/pmaovr/Predicting-Missing-Aspects-of-Vulnerability-Reports}{\textcolor{blue}{the Github repository}}).
First, based on a advantageous matching technique ``Gazetteer''~\cite{ner0,ner1}, we build a gazetteer commonly used for vulnerability type, root cause, impact, attacker type and attack vector, as well as a gazetteer for product and vendor names from \href{https://www.cvedetails.com/}{\textcolor{blue}{CVE Details}}.
We also define sentence-level patterns that represent the common appearance order of different aspects in CVE descriptions.


We adopt Stanford CoreNLP~\cite{stanf} to parse a CVE description and obtain POS tags of this  sentence.
Next, we combine gazetteer matching and POS pattern matching to decide the candidates of certain CVE aspects.
Finally, we examine the candidates against the two official description templates and other general sentence-level patterns in order to filter out false positive aspect candidates
For example, attacker type, impact and attack vector often appear together in the form of ``allow [attacker type] to [impact] via [attack vector]'' or ''[attacker type] performs [attack vector] in order to [impact]''.
Meanwhile, ``executing the script'' can belong to either Attack vector or Impact aspect, while it is exactly an Attack vector when it appears in the sentence ``By executing the script ...''.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Accuracy of CVE Aspect Extraction}
\label{sec:extractionquality}

\begin{table}%[h]
	\centering
%	\setlength{\abovecaptionskip}{0.25cm}%    
%	\setlength{\belowcaptionskip}{0cm}%
	\caption{Variations of the extracted aspect descriptions.}
	\vspace{-3mm}
	\scriptsize
	\setlength{\tabcolsep}{0mm}{
		\begin{tabular}{clccc}
			\toprule
			Aspect&CVE-ID&Description\\
			\midrule
			\multirow{3}{*}{Vulnerability Type}&CVE-2017-11507&cross-site scripting (XSS) vulnerability \\
			&CVE-2018-16481&XSS vulnerability \\
			&CVE-2018-10937&cross site scripting flaw \\
			\midrule
			\multirow{3}{*}{Root cause} &CVE-2008-1419&does not properly handle ...\\
			&CVE-2010-0027&does not properly process ...\\
			&CVE-2015-1992&improperly processes ...\\
			\midrule
			\multirow{3}{*}{Affected product}&CVE-2006-3500&The dynamic linker (dyld) in Apple ...\\
			&CVE-1999-0786&The dynamic linker in Solaris \\
			&CVE-2013-0977&dyld in Apple iOS before 6.1.3 and Apple TV ... \\
			\midrule
			\multirow{3}{*}{Impact}&CVE-2011-4129&obtain sensitive information \\
			&CVE-2005-2436&obtain sensitive data\\
			&CVE-2002-0257&obtain information from other users\\
			\midrule
			\multirow{3}{*}{Attacker type} &CVE-2018-1000634&user with privilege\\
			&CVE-2018-1000084&low privilege user \\
			&CVE-2016-9603&A privileged user/process\\
			\midrule
			\multirow{3}{*}{Attack vector}&CVE-2018-12581&use a crafted database name\\
			&CVE-2019-11768& a specially crafted database name can be used\\
			&CVE-2012-1190&via a crafted database name\\
			
			\bottomrule
		\end{tabular}}
	\label{tab:aspdes}
	\vspace{-5mm}
\end{table}


We apply our aspect detection tool to the rest 96,081 CVEs.
We extract 41003, 15129, 92132, 89053, 73134 and 67188 instances of the vulnerability type, root cause, affected product, impact, attacker type and attack vector, respectively.
Considering large numbers of instances to examine, we adopt a statistical sampling met\-hod~\cite{samp} to evaluate the accuracy of the extracted aspect instances.
Specifically, we sample and examine the minimum number MIN of data instances. %in order to ensure that the estimated accuracy.
MIN is determined by $n_0/(1+(n_0-1)/populationsize)$ where $n_0 = (Z^2*0.25)/e^2$, and $Z$ is the confidence level's z-score and $e$ is the error margin.
In this work, we consider 5\% error margin at 95\% confidence level.
At this setting, we examine 384 extracted instances for each aspect.
One author labels the sampled instances, and the other author validates the results.
The two authors discuss to resolve the disagreement.
The extraction accuracy is 97\%, 96\%, 96\%, 98\%, 99\% and 98\% for the vulnerability type, root cause, affected product, impact, attacker type and attack vector, respectively.
Table~\ref{tab:aspdes} shows some examples of the extracted aspect phrases. 
We can see that our aspect extraction method is flexible to handle the variations of aspect extraction.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Missing and Distribution of CVE Aspects}
\begin{table}%[h]
	\centering
%	\setlength{\abovecaptionskip}{-0cm}%    
	%\setlength{\belowcaptionskip}{0cm}%
	\caption{Class distribution of CVE aspects.}
	\vspace{-3mm}
	\scriptsize
	\setlength{\tabcolsep}{0mm}{
		\begin{tabular}{cccc}
			\toprule
			\multicolumn{4}{c}{\textbf{Vulnerability Type}}\\
			\midrule
			Cross site scripting(CWE-79)&29.5\%&SQL injection(CWE-89)&17.8\%\\
			Buffer Overflow(CWE-119)&17.1\%&Directory Traversal(CWE-32)&8.9\%\\
			Cross-site request forgery(CWE-352)&7.1\%&PHP file inclusion(CWE-98)&5.7\%\\
			Use-after-free(CWE-416)&3.2\%&Integer overflow(CWE-680)&2.6\%\\
			Untrusted search path(CWE-426)&1.7\%&Format string(CWE-134)&1.6\%\\
			CRLF injection(CWE-93)&0.6\%&XML External Entity(CWE-661)&0.3\%\\
			Others&4.0\%\\
			\toprule
			\multicolumn{4}{c}{\textbf{Root Cause}}\\
			\midrule
			Input Validation Error&51.7\%&Boundary Condition Error&24.5\%\\
			Failure to Handle Exceptional Conditions&11.7\%&Design Error&11.0\%\\
			Access Validation Error&0.7\%&Atomicity Error&0.1\%\\
			Race Condition Error&0.1\%&Serialization Error&0.1\%\\
			Configuration Error&0.1\%& Origin Validation Error&0.1\%\\
			Environment Error&0.1\%\\
			\toprule
			\multicolumn{4}{c}{\textbf{Attack Vector}}\\
			\midrule
			Via field, arguments or parameter&51.7\%&Via some crafted data&17.1\%\\
			By executing the script&14.0\%&HTTP protocol correlation&4.4\%\\
			Call API&3.3\%&Others&8.0\%\\
			\toprule
			\multicolumn{4}{c}{\textbf{Attacker Type}}\\
			\midrule
			Remote attacker&72.8\%&Local attacker&11.1\%\\
			Authenticated user&8.1\%&Context-dependent&2.9\%\\
			Physically proximate attacker&0.3\%&Others&4.7\%\\	
			\bottomrule
		\end{tabular}}
	\label{tab:freq}
	\vspace{-5mm}
\end{table}

Based on the extracted CVE aspects (which are of high accuracy), we analyze the missing of key aspects in the CVE descriptions.
We observe different severities of information missing for different aspects.
About 43.8\% of CVEs describe vulnerability type.
Only about 15.2\% of CVEs describes root cause.
About 3\% of CVEs describe both vulnerability type and root cause.
62\% of CVEs describe attack vector, and 72\% of CVEs describe attacker type.
Almost all (over 99\%) CVEs describe affected product, and about 94\% of CVEs describe impact.
We find that about 31\% of CVE miss one aspect, 39\% missing two, and 28\% miss three or more aspects.
Table~\ref{tab:freq} lists the class distributions of the extracted aspects.
We do not include affected product and impact because they do not suffer from significant information missing.
We can see that the class distribution of an aspect is in general imbalance, which has several frequent classes and many less frequent classes in the long tail.
We group the classes with the frequencies $<0.1$ as Others.

